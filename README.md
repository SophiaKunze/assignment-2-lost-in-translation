# Lost in Translation - an Online Sign Language Translator

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/Readme-Standard-green?logo=markdown)](https://github.com/RichardLitt/standard-readme)
[![FlatIcon](https://img.shields.io/badge/Images-FlatIcon-blue)](https://www.flaticon.com/)
[![React](https://img.shields.io/badge/-React-black?logo=react)](https://reactjs.org/)
[![JavaScript](https://img.shields.io/badge/-JavaScript-black?logo=javascript)](https://developer.mozilla.org/en-US/docs/web/javascript)


This is a dynamic webpage using React.

This repository contains:

1. [lost-in-translation-component-tree.pdf](lost-in-translation-component-tree.pdf) displaying components structure
2. [package-lock.json](package-lock.json) listing versions of installed packages
3. [package.json](package.json) listing metadata, dependencies and scripts
4. [src](src) containing the source code
5. [public](public) containing static files such as index.html and images


## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This app is an online sign language translator as a Single Web Page Application using the React framework. It is an assignment in the scope of a fullstack development course by <a href="https://www.noroff.no/en/">Noroff</a>. A user can login and translate english letters to American Sign Language. Username and translated words are stored in an api. The user has the option to delete translated words from the api and to logout.

## Install
Clone repository via `git clone` and install dependencies via `npm install`.

## Usage
`npm start` to run a local React server or visit application hosted on [Heroku](https://lost-in-translation-sophia.herokuapp.com/).

## Maintainers

[@SophiaKunze](https://gitlab.com/SophiaKunze).

## Contributing
Feel free to dive in! [Open an issue](https://gitlab.com/SophiaKunze/assignment-2-lost-in-translation/-/issues/new).

This projects follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.


### Acknowledgement

This project exists thanks to my teacher <a href="https://gitlab.com/sumodevelopment">Dewald Els</a> and <a href=https://www.experis.de/de>Experis</a>.

## License

[MIT](https://opensource.org/licenses/MIT) © Sophia Kunze
