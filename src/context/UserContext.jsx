import { createContext, useState, useContext } from "react";
import { storageRead } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKeys";

// Context -> exposing state
const UserContext = createContext();

// custom hook
export const useUser = () => {
  return useContext(UserContext); // returns {user, setUser}
};

// Provider -> managing state
const UserProvider = ({ children }) => {
  // if user is logged in already they will be redirected to profile page
  const [user, setUser] = useState(storageRead(STORAGE_KEY_USER));
  const state = {
    user,
    setUser,
  };

  return <UserContext.Provider value={state}>{children}</UserContext.Provider>;
};
export default UserProvider;
