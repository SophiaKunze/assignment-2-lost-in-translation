// AppContext collects all contexts and wraps them
import UserProvider from "./UserContext";

// children are destructured from props (the argument of the arrow function)
const AppContext = ({ children }) => {
  return <UserProvider>{children}</UserProvider>;
};

export default AppContext;
