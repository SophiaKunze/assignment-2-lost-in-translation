import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import BootstrapNavbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import { storageDelete } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { Col, Row } from "react-bootstrap";
import { LOGIN_ROOT, PROFILE_ROOT, TRANSLATION_ROOT } from "../../const/roots";

const Navbar = () => {
  const { user, setUser } = useUser();

  const isLoggedIn = user !== null;

  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      // delete user from local storage
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  return (
    <BootstrapNavbar className="yellow-background" expand="md">
      <Container>
        {/* if user is not logged in, link to / page. if user is logged in, link to translations. */}
        <BootstrapNavbar.Brand
          as={NavLink}
          to={isLoggedIn ? TRANSLATION_ROOT : LOGIN_ROOT}
        >
          <Row>
            <Col>
              {isLoggedIn && (
                <img
                  alt=""
                  src="/images/Logo.png"
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                />
              )}
            </Col>
            <Col>
              <h4>Lost in Translation</h4>
            </Col>
          </Row>
        </BootstrapNavbar.Brand>

        {/** Make sure user only sees navigation to profile and translation if they is logged in */}
        {isLoggedIn && (
          <>
            <BootstrapNavbar.Toggle aria-controls="navbar-nav" />
            <BootstrapNavbar.Collapse id="navbar-nav">
              <Nav className="me-auto">
                {/* use Navlink or as={NavLink} and not an anchor tag to prevent page from reloading */}
                <Nav.Link as={NavLink} to={TRANSLATION_ROOT}>
                  Translations
                </Nav.Link>
                <Nav.Link as={NavLink} to={PROFILE_ROOT}>
                  Profile
                </Nav.Link>
              </Nav>
              <Button className="square-button" onClick={handleLogoutClick}>
                Logout
              </Button>
            </BootstrapNavbar.Collapse>
          </>
        )}
      </Container>
    </BootstrapNavbar>
  );
};

export default Navbar;
