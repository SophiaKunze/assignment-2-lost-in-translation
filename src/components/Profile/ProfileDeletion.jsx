import { storageSave } from "../../utils/storage";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { translationClearHistory } from "../../api/translate";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";

const ProfileDeletion = () => {
  const { user, setUser } = useUser();

  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis cannot be undone!")) {
      return;
    }
    const [clearError] = await translationClearHistory(user.id);

    if (clearError !== null) {
      return;
    }

    const updatedUser = {
      ...user,
      translations: [],
    };

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  return (
    <Container>
      <Button
        type="button"
        className="square-button"
        onClick={handleClearHistoryClick}
      >
        Clear history
      </Button>
    </Container>
  );
};
export default ProfileDeletion;
