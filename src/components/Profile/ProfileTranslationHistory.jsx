import Card from "react-bootstrap/Card";
import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";
import ProfileDeletion from "./ProfileDeletion";
import { useUser } from "../../context/UserContext";

const ProfileTranslationHistory = ({ translations }) => {
  const { user, setUser } = useUser();

  const translationList = translations.map((translation, index) => (
    <ProfileTranslationHistoryItem
      key={index + "-" + translation}
      translation={translation}
    />
  ));

  return (
    <Card>
      <Card.Header>Hello, {user.username}</Card.Header>
      <Card.Body>
        {translationList.length !== 0 && (
          <Card.Text>Your translations history</Card.Text>
        )}
        {translationList.length === 0 && (
          <Card.Text>You have no translations yet.</Card.Text>
        )}
        <ul>{translationList}</ul>
      </Card.Body>
      <ProfileDeletion />
    </Card>
  );
};
export default ProfileTranslationHistory;
