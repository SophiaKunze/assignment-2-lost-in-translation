import Container from "react-bootstrap/Container";

const ProfileHeader = ({ username }) => {
  return (
    <Container>
      <header>
        <h4>Hello, welcome back {username}</h4>
      </header>
    </Container>
  );
};
export default ProfileHeader;
