import ListGroup from "react-bootstrap/ListGroup";
import Container from "react-bootstrap/Container";

const ProfileTranslationHistoryItem = ({ translation }) => {
  return (
    <Container>
      <ListGroup.Item>{translation}</ListGroup.Item>
    </Container>
  );
};
export default ProfileTranslationHistoryItem;
