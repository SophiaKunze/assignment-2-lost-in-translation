import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useForm } from "react-hook-form";
import "../../index.css";
import Container from "react-bootstrap/Container";
import TranslationsButton from "./TranslationsButton";

const translationConfig = {
  required: true, // input is required
  pattern: /^[a-zA-Z ]+$/, // accept letters and whitespace
  maxLength: 40, // length of input is restricted
  setValueAs: (text) => text.trim().toLowerCase(), // is called before other checks are done and prevents empty text
};

const TranslationsForm = ({ onTranslate }) => {
  const {
    register,
    handleSubmit,
    // these errors are related to invalid user input
    formState: { errors },
  } = useForm();

  const onSubmit = ({ translationText }) => {
    onTranslate(translationText);
  };

  const errorMessage = (() => {
    // optional chaining to prevent read property on undefined (TypeError)
    if (errors.translationText?.type === "pattern") {
      return <span>Enter English letters only</span>;
    }
    if (errors.translationText?.type === "maxLength") {
      return (
        <span>Enter no more than {translationConfig.maxLength} letters</span>
      );
    }
    // non-breaking space causes react to render empty space
    return <span> &nbsp; </span>;
  })();

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Form.Group className="mb-3" controlId="formBasicText">
          <Row>
            <Col>
              <Form.Label></Form.Label>
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 10, offset: 0 }} md={{ span: 5, offset: 3 }}>
              <Form.Control
                type="text"
                placeholder="Enter text here"
                {...register("translationText", translationConfig)}
              />
            </Col>
            <Col xs={{ span: 2 }} md={{ span: 1 }}>
              <TranslationsButton />
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 10, offset: 0 }} md={{ span: 5, offset: 3 }}>
              {errorMessage}
            </Col>
          </Row>
        </Form.Group>
      </Form>
    </Container>
  );
};
export default TranslationsForm;
