import { nanoid } from "nanoid";
import Card from "react-bootstrap/Card";

const TranslationsSummary = ({ translationText }) => {
  // remove white spaces and split into array
  const translationArray = translationText.replace(/\s+/g, "").split("");
  return (
    <Card>
      <Card.Body>
        {/** Translate text into images */}
        {translationArray.map((character) => (
          <img
            src={`images/${character}.png`}
            width="10%"
            alt={character}
            key={nanoid()}
          />
        ))}
      </Card.Body>
    </Card>
  );
};

export default TranslationsSummary;
