import "../../index.css";

const TranslationsButton = () => {
  return (
    <input
      type="image"
      src={"images/submit-arrow.svg"}
      alt="Translate"
      className="submit-button"
    />
  );
};

export default TranslationsButton;
