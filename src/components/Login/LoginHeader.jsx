import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";

const LoginHeader = () => {
  return (
    <Container>
      <Row>
        <Col md={{ span: 3, offset: 0 }}>
          <Image
            src="images/Logo-Hello.png"
            className="logo-hello"
            alt="hello"
          />
        </Col>
        <Col md={{ span: 5, offset: 0 }}>
          <h1>Lost in Translation</h1>
          <h4>Get started</h4>
        </Col>
      </Row>
    </Container>
  );
};

export default LoginHeader;
