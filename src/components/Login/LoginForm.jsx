import { useForm } from "react-hook-form";
import { userLogin } from "../../api/user";
import { useState, useEffect } from "react";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import LoginButton from "./LoginButton";
import Loading from "./Loading";

// static -> defined outside LoginForm so that it won't rerender every time
const usernameConfig = {
  required: true,
  minLength: 3,
  pattern: /^[a-zA-Z0-9 ]+$/, // accept letters and whitespace
  maxLength: 10, // length of input is restricted
  setValueAs: (name) => name.trim(), // is called before other checks are done and prevents empty name
};

const LoginForm = () => {
  /* Hooks */
  const {
    register,
    handleSubmit,
    // these errors are related to invalid user input
    formState: { errors },
  } = useForm();

  const { user, setUser } = useUser();

  const navigate = useNavigate();

  /* Local State */
  const [loading, setLoading] = useState(false);
  // apiError if some error occurs while logging in
  const [apiError, setApiError] = useState(null);

  /* Side Effects */
  useEffect(() => {
    if (user !== null) {
      // page where logged in user is directed to
      navigate("translations");
    }
  }, [user, navigate]); // empty dependencies means that code only run once

  /* Event Handlers */
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await userLogin(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  /* Render Functions */
  const errorMessage = (() => {
    // optional chaining to prevent read property on undefined (TypeError)
    if (errors.username?.type === "required") {
      return <span>Enter username</span>;
    }
    if (errors.username?.type === "minLength") {
      return <span>Enter at least {usernameConfig.minLength} characters</span>;
    }
    if (errors.username?.type === "maxLength") {
      return (
        <span>Enter no more than {usernameConfig.maxLength} characters</span>
      );
    }
    if (errors.username?.type === "pattern") {
      return <span>Enter English letters and numbers only</span>;
    }
    // non-breaking space causes react to render empty space
    return <span> &nbsp; </span>;
  })();

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Form.Group controlId="formBasicText">
          <Row>
            <Col>
              <Form.Label></Form.Label>
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 10, offset: 0 }} md={{ span: 5, offset: 3 }}>
              <Form.Control
                type="text"
                placeholder="What's your name?"
                {...register("username", usernameConfig)}
              />
            </Col>
            <Col
              className={`${loading && "loading-spinner "}justify-content-left`}
              xs={{ span: 2 }}
              md={{ span: 1 }}
            >
              {loading || <LoginButton loading={loading} />}
              {loading && <Loading />}
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 10, offset: 0 }} md={{ span: 5, offset: 3 }}>
              {errorMessage}
            </Col>
          </Row>
        </Form.Group>
        {apiError && <p>{apiError}</p>}
      </Form>
    </Container>
  );
};
export default LoginForm;
