import "../../index.css";

const LoginButton = ({ loading }) => {
  return (
    <input
      type="image"
      disabled={loading}
      src={"images/submit-arrow.svg"}
      alt="Login"
      className="submit-button"
    />
  );
};

export default LoginButton;
