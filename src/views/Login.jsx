import Container from "react-bootstrap/Container";
import LoginForm from "../components/Login/LoginForm";
import LoginHeader from "../components/Login/LoginHeader";

const Login = () => {
  return (
    <div className="yellow-background">
      <Container>
        <LoginHeader />
        <LoginForm />
      </Container>
    </div>
  );
};

export default Login;
