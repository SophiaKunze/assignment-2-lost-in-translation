import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";

const Profile = () => {
  const { user, setUser } = useUser();

  return (
    <>
      <ProfileHeader username={user.username} />
      <ProfileTranslationHistory translations={user.translations.slice(-10)} />
    </>
  );
};
// wrapping in withAuth protects root
export default withAuth(Profile);
