import TranslationsForm from "../components/Translations/TranslationsForm";
import withAuth from "../hoc/withAuth";
import { useUser } from "../context/UserContext";
import { translationAdd } from "../api/translate";
import { storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useState } from "react";
import TranslationsSummary from "../components/Translations/TranslationsSummary";

const Translations = () => {
  const { user, setUser } = useUser();
  const [currentText, setCurrentText] = useState("");

  const handleTranslateClicked = async (translationText) => {
    setCurrentText(translationText);
    // send http request
    const [error, updatedUser] = await translationAdd(user, translationText);
    if (error !== null) {
      return;
    }
    // keep UI state and server state in sync
    storageSave(STORAGE_KEY_USER, updatedUser);
    // update context state
    setUser(updatedUser);
  };

  return (
    <>
      <section id="translationText">
        <TranslationsForm onTranslate={handleTranslateClicked} />
      </section>
      {<TranslationsSummary translationText={currentText} />}
    </>
  );
};
// wrapping in withAuth protects root
export default withAuth(Translations);
