import { useUser } from "../context/UserContext";
import { Navigate } from "react-router-dom";
import { LOGIN_ROOT } from "../const/roots";

// higher order component for protecting roots
const withAuth = (Component) => (props) => {
  const { user } = useUser();
  if (user !== null) {
    return <Component {...props} />;
  } else {
    return <Navigate to={LOGIN_ROOT} />;
  }
};

export default withAuth;
