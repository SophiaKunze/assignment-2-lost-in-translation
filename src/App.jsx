import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./views/Login";
import Translations from "./views/Translations";
import Profile from "./views/Profile";
import Navbar from "./components/Navbar/Navbar";
import { LOGIN_ROOT, PROFILE_ROOT, TRANSLATION_ROOT } from "./const/roots";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path={LOGIN_ROOT} element={<Login />} />
          <Route path={TRANSLATION_ROOT} element={<Translations />} />
          <Route path={PROFILE_ROOT} element={<Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
