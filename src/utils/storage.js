const validateKey = (key) => {
  if (!key || typeof key !== "string") {
    throw new Error("No valid storage key provided");
  }
};

export const storageSave = (key, value) => {
  validateKey(key);
  if (!value) {
    throw new Error("storageSave: No value provided for " + key);
  }
  // global object localStorage in a built-in browser api
  // in production level app we would not expose user id to localStorage (identification of user by token instead)
  sessionStorage.setItem(key, JSON.stringify(value));
};

export const storageRead = (key) => {
  validateKey(key);

  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};

export const storageDelete = (key) => {
  validateKey(key);
  sessionStorage.removeItem(key);
};
